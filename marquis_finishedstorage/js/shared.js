// Shared code needed by the code of all three pages.

// Prefix to use for Local Storage.  You may change this.
var APP_PREFIX = "monash.eng1003.fencingApp";

// Array of saved Region objects.
class Region {
    constructor(regionId){
        this._location = newpostion;
        this._SaveDataTime = new Date();
        this._SaveRegionId = regionId;
        this._area = areaCal;
        this._perimeters = periCal;
        
    }
    getarea(){
        let p = geod.Polygon();
        this._locations.forEach(point => {
            p.AddPoint(point[1], point[0]);
        });
        return p.Compute(true, true).area.toFixed(2);
    }
    getperimeters(){
        let p = geod.Polygon();
        this._locations.forEach(point => {
            p.AddPoint(point[1], point[0]);
        });
        return p.Compute(true, true).perimeter.toFixed(2) + " m";
    }
}

class RegionList {
    constructor(){
        this._Regions = [];
        this._NumberOfRegions = 0;
       }
    
    addRegion(region) {
        this._Regions.push(region);
    }  
    getNumberOfRegions() {
        return this._Regions.length;
    }
    orderOfRegions(index){
        return this._Regions[index]
    }
    newNumberOfRegions(regionlist){
        this._NumberOfRegions = regionlist
    }
    transforData(region,numOfRegion){
        this._Regions=region
        this._NumberOfRegions=numOfRegion
    }
}