// Code for the View Region page.

// The following is sample code to demonstrate navigation.
// You need not use it for final app.


mapboxgl.accessToken = 'pk.eyJ1IjoieHVuemhhbmciLCJhIjoiY2tqZ3pzZHR6M2gzYTJycDk2anFvNHFuYSJ9.IvnWT5teVwRTkqYAjyrcuw';

var regionIndex = localStorage.getItem(APP_PREFIX + "-selectedRegion"); 
console.log(regionIndex)
var loadStorage = JSON.parse(localStorage.getItem("regionList"))
var regionList = loadStorage._Regions
let selectedRegion = regionList[regionIndex];
var selectedLocation = selectedRegion._location
var addOne = selectedLocation
addOne.push(selectedLocation[0])
console.log(addOne)
if (regionIndex !== null)
{
    // If a region index was specified, show name in header bar title. This
    // is just to demonstrate navigation.  You should set the page header bar
    // title to an appropriate description of the region being displayed.
    var regionNames = [  ];
    for(i=0;i<regionList.length;i++){
        regionNames.push(regionList[i]._SaveRegionId)
    }
    document.getElementById("headerBarTitle").textContent = regionNames[regionIndex];
}
let map = new mapboxgl.Map({
    container: "map",
    style: "mapbox://styles/mapbox/streets-v11",
    zoom: 18,
    center: selectedLocation[0]
});

if ("geolocation" in navigator) { 
    navigator.geolocation.getCurrentPosition(position => { 
         map.jumpTo({
         // initial position in [lon, lat] format
          center: [position.coords.longitude, position.coords.latitude],
        });
        
        var popup = new mapboxgl.Popup({ closeOnClick: false })
            .setLngLat([position.coords.longitude, position.coords.latitude])
            .setHTML('here you are')
            .addTo(map);
        
        new mapboxgl.Marker()
            .setLngLat([position.coords.longitude, position.coords.latitude])
            .setPopup(popup) // sets a popup on this marker
            .addTo(map);
    }); 
} else { /* geolocation IS NOT available, handle it */
        map.jumpTo({
        center: Clayton, // starting position [Clayton]
    });}



map.on('load', function () {
map.addSource('route', {
'type': 'geojson',
'data': {
'type': 'Feature',
'properties': {},
'geometry': {
'type': 'LineString',
'coordinates': addOne
}
}
});
map.addLayer({
'id': 'route',
'type': 'line',
'source': 'route',
'layout': {
'line-join': 'round',
'line-cap': 'round'
},
'paint': {
'line-color': '#888',
'line-width': 8
}
});
});


map.on('load', function () {
map.addSource('maine', {
'type': 'geojson',
'data': {
'type': 'Feature',
'geometry': {
'type': 'Polygon',
'coordinates': [addOne]
}
}
});
map.addLayer({
'id': 'maine',
'type': 'fill',
'source': 'maine',
'layout': {},
'paint': {
'fill-color': '#088',
'fill-opacity': 0.8
}
});
});

 
document.getElementById("RemoveRegion").addEventListener("click", function (e) {

    if (confirm("Are you sure you want to remove this region?")) {
        loadStorage._Regions.splice(regionIndex, 1)
        loadStorage._NumberOfRegions-=1;
        localStorage.setItem("regionList", JSON.stringify(loadStorage));
        window.location = "index.html";
    }
});



            



                

window.onload = function (e) {
    document.getElementById("perimeterValue").innerText = selectedRegion._perimeters;
    document.getElementById("areaValue").innerText = selectedRegion._area;
}
