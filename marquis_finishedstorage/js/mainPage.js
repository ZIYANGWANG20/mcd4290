// Code for the main app page (Regions List).

// The following is sample code to demonstrate navigation.
// You need not use it for final app.
function viewRegion(regionIndex)
{
    // Save the desired region to local storage so it can be accessed from view region page.
    localStorage.setItem(APP_PREFIX + "-selectedRegion", regionIndex); 
    // ... and load the view region page.
    location.href = 'viewRegion.html';
}

function AAA(){   
    let loadObject = JSON.parse(localStorage.getItem("regionList"))
    var regionlist= new RegionList()
    regionlist.transforData(loadObject._Regions,loadObject._NumberOfRegions)
        let listHTML = "";
    console.log(loadObject)
    console.log(regionlist)
     console.log(regionlist.getNumberOfRegions());
    for (let i = 0;i < regionlist.getNumberOfRegions();i++){
    let seeRegion = regionlist.orderOfRegions(i);
    listHTML += `<li class="mdl-list__item mdl-list__item--two-line" onclick="viewRegion(${i});">
<span class="mdl-list__item-primary-content">
<span>Region ${i+1} ${seeRegion._SaveRegionId}</span>
<span class="mdl-list__item-sub-title">${seeRegion._SaveDateTime}</span>
</span>
</li>`
    }
    document.getElementById("regionsList").innerHTML = listHTML

}
AAA()