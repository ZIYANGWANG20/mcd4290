// Code for the Record Region page.
let newpostion =[]
let i= -1
let countLocation = 0
let positionsNum =[]
let geod = GeographicLib.Geodesic.WGS84;
var areaCal
var periCal
function counter(){
    i+=1
}
mapboxgl.accessToken = 'pk.eyJ1IjoieHVuemhhbmciLCJhIjoiY2tqZ3pzZHR6M2gzYTJycDk2anFvNHFuYSJ9.IvnWT5teVwRTkqYAjyrcuw';

let Clayton = [145.128286998,-37.912268340];
let map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/mapbox/streets-v11',
    center : Clayton,
    zoom: 14
});

if ("geolocation" in navigator) { 
    navigator.geolocation.getCurrentPosition(position => { 
         map.jumpTo({
         // initial position in [lon, lat] format
          center: [position.coords.longitude, position.coords.latitude],
        });
        
        var popup = new mapboxgl.Popup({ closeOnClick: false })
            .setLngLat([position.coords.longitude, position.coords.latitude])
            .setHTML('here you are')
            .addTo(map);
        
        new mapboxgl.Marker()
            .setLngLat([position.coords.longitude, position.coords.latitude])
            .setPopup(popup) // sets a popup on this marker
            .addTo(map);
    }); 
} else { /* geolocation IS NOT available, handle it */
        map.jumpTo({
        center: Clayton, // starting position [Clayton]
    });}

map.on('mousemove', function (e) {
// e.point is the x, y coordinates of the mousemove event relative
// to the top-left corner of the map
// e.lngLat is the longitude, latitude geographical position of the event
document.getElementById('info').innerHTML = JSON.stringify(e.point) +
'<br />' + JSON.stringify(e.lngLat.wrap());
});
function showPath()
        {
            // Code added here will run when the "Show Path" button is clicked.
        }
    
        function showPolygon()
        {
            // Code added here will run when the "Show Polygon" button is clicked.
        }

        // This function checks whether there is a map layer with id matching 
        // idToRemove.  If there is, it is removed.
        function removeLayerWithId(idToRemove)
        {
            let hasPoly = map.getLayer(idToRemove)
            if (hasPoly !== undefined)
            {
                map.removeLayer(idToRemove)
                map.removeSource(idToRemove)
            }
        }
/*var draw = new MapboxDraw({
displayControlsDefault: false,
controls: {
polygon: true,
trash: true
}
});
map.addControl(draw);
 
map.on('draw.create', updateArea);
map.on('draw.delete', updateArea);
map.on('draw.update', updateArea);
 
function updateArea(e) {
var data = draw.getAll();
var answer = document.getElementById('calculated-area');
if (data.features.length > 0) {
var area = turf.area(data);
// restrict to area to 2 decimal points
var rounded_area = Math.round(area * 100) / 100;
answer.innerHTML =
'<p><strong>' +rounded_area +'  square meters</p>';
} else {
answer.innerHTML = '';
if (e.type !== 'draw.delete')
alert('Use the draw tools to draw a polygon!');
}
}*/
var area = new PolygonArea
function AddCorners(){
    map.on('click', function(e) {
// The event object (e) contains information like the
// coordinates of the point on the map that was clicked.
        counter()
        //console.log('A click event has occurred at ' + e.lngLat);
        newpostion.push(e.lngLat)
        //console.log(positionsNum)
        new mapboxgl.Popup()
                  .setLngLat(e.lngLat)
                  .setHTML([parseFloat(newpostion[i]['lng']),parseFloat(newpostion[i]['lat'])])
                  .addTo(map);

        if(i<1){
            positionsNum.push([parseFloat((parseFloat(newpostion[0]['lng'])).toFixed(3)),parseFloat((parseFloat(newpostion[0]['lat'])).toFixed(3))])
            console.log(positionsNum)

        }
        else{
            positionsNum.pop()
            positionsNum.push([parseFloat((parseFloat(newpostion[i]['lng'])).toFixed(3)),parseFloat((parseFloat(newpostion[i]['lat'])).toFixed(3))])
            positionsNum.push([parseFloat((parseFloat(newpostion[0]['lng'])).toFixed(3)),parseFloat((parseFloat(newpostion[0]['lat'])).toFixed(3))])
            console.log(positionsNum)

        }
 
});


    map.on('click', function () {
map.addSource(`route${i}`, {
'type': 'geojson',
'data': {
'type': 'Feature',
'properties': {},
'geometry': {
'type': 'LineString',
'coordinates': positionsNum}}});
         //map.removeLayer(`route${i}`);
    map.addLayer({
'id': `route${i}`,
'type': 'line',
'source': `route${i}`,
'layout': {
'line-join': 'round',
'line-cap': 'round'
},
'paint': {
'line-color': '#999',
'line-width': 5
}
});
if(i>2){
    map.removeLayer(`route${i-1}`);
    map.removeLayer(`polygon${i-1}`);
}
    map.addSource(`polygon${i}`, {
'type': 'geojson',
'data': {
'type': 'Feature',
'geometry': {
'type': 'Polygon',
'coordinates': [positionsNum]
}
}
});
map.addLayer({
'id': `polygon${i}`,
'type': 'fill',
'source': `polygon${i}`,
'layout': {},
'paint': {
'fill-color': '#088',
'fill-opacity': 0.8
}
});       
});
}


function RemoveCorners(newpostion,positionsNum){
    //let times = 0
    let j = i
    let clear_pos=positionsNum
    let clear_lnglat=newpostion
    console.log(clear_pos)
    clear_pos.splice(0,clear_pos.length)
    clear_lnglat.splice(0,clear_lnglat.length)
    console.log(i)
    map.removeLayer(`polygon${j}`);
    map.removeLayer(`route${j}`);
    let k=1
    do{
        map.removeLayer(`polygon${k}`);
        map.removeLayer(`route${k}`);
        k-=1
    }while(k>=0)
    do{
        //map.removeLayer(`polygon${j}`);
        //map.removeLayer(`route${j}`);
        map.removeSource(`polygon${j}`)
        map.removeSource(`route${j}`);
        j-=1
    }while(j>=0)
        i=-1
        newpostion =[]
    positionsNum =[]
    
    console.log(clear_pos)
    
}

function tryit(positionsNum){
    positionsNum.pop
    let geod = GeographicLib.Geodesic.WGS84;
    //console.log(geod)
    let p = new GeographicLib.PolygonArea.PolygonArea(geod),qwe=positionsNum;
   /* var p = geod.Polygon(false), g,
    qwe=[[-122.1, -30],[-123.2, -31],[-124.3, -32],[-121.4, -31]]
    //[[-63.1, -58], [-72.9, -74], [-71.9,-102], [-74.9,-102]]
;*/
    console.log(qwe)
for (g = 0; g < qwe.length; ++g)
p.AddPoint(qwe[g][1],qwe[g][0]);
p = p.Compute(true, true);
    
//console.log(positionsNum)
    periCal=p.perimeter.toFixed(3)
    areaCal = p.area.toFixed(1)
    alert("Perimeter/area of region are " +
            p.perimeter.toFixed(1) + " m / " +
            p.area.toFixed(3) + " m^2.")
console.log("Perimeter/area of region are " +
            p.perimeter.toFixed(1) + " m / " +
            p.area.toFixed(3) + " m^2.");}

/*function calculateData(lat,lon){
    //Clear()
    console.log(lat)
    console.log(lon)
    console.log(area)
}*/

function reset(){
    //tryit(positionsNum)
    RemoveCorners(newpostion,positionsNum)
    
}
function saveRegion(){
     tryit(positionsNum)
     regionId = prompt("Please enter an ID for this region: ");
    var new_region = new Region(regionId);
    var region_list = Retrieve();
    new_region._location=positionsNum
    region_list._Regions.push(new_region);
    region_list._NumberOfRegions=region_list._Regions.length
    Serialization(region_list);
    window.location.href = "index.html";//return to index.html page
}

function Retrieve(){
    var regionString = localStorage.getItem("regionList") || "";
    var region_list= new RegionList()
    if(regionString!=""){
        region_list = JSON.parse(regionString);
        
    }
    
    else {
        region_list._Regions=[];}
    return region_list;
}//store the user input region into the regionlist
function Serialization(region_list){
    localStorage.setItem("regionList",JSON.stringify(region_list));
    alert("Create Region Successfully");
}//store the regionlist into local storage
