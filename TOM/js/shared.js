// Shared code needed by the code of all three pages.

// Prefix to use for Local Storage.  You may change this.
var APP_PREFIX = "monash.eng1003.fencingApp";

// Array of saved Region objects.
var savedRegions = [];

class Region {
    constructor(id){
        this._location = [];
        this._SaveDataTime = new Date();
        this._SaveRegionId = id;
    }
}

class RegionList {
    constructor(){
        this._Regions = [];
        this._NumberOfRegions = 0;
       }
    
    addRegion(region) {
        this._Regions.push(region);
    }
    
    GetRegion(id) {
        return this._Regions.find(r => r._SaveRegionId == id);
    }
    
    RemoveRegion(id) {
        this._Regions.slice(a => a._SaveRegionId == id, 1);
    }
    
    GetNumberOfRegions() {
        return this._Regions.length;
    }
}