// Code for the Record Region page.

mapboxgl.accessToken = 'pk.eyJ1IjoieHVuemhhbmciLCJhIjoiY2tqZ3pzZHR6M2gzYTJycDk2anFvNHFuYSJ9.IvnWT5teVwRTkqYAjyrcuw';

let Clayton = [145.128286998,-37.912268340];
let map;

if ("geolocation" in navigator) { 
    navigator.geolocation.getCurrentPosition(position => { 
         map = new mapboxgl.Map({
        // container id specified in the HTML
          container: 'map',

           // style URL
          style: 'mapbox://styles/mapbox/streets-v11',

         // initial position in [lon, lat] format
          center: [position.coords.longitude, position.coords.latitude],

         // initial zoom

         zoom: 14
        });
        
        var popup = new mapboxgl.Popup({ closeOnClick: false })
            .setLngLat([position.coords.longitude, position.coords.latitude])
            .setHTML('here you are')
            .addTo(map);
        
        new mapboxgl.Marker()
            .setLngLat([position.coords.longitude, position.coords.latitude])
            .setPopup(popup) // sets a popup on this marker
            .addTo(map);
    }); 
} else { /* geolocation IS NOT available, handle it */
        map = new mapboxgl.Map({
        container: 'map', // container id
        style: 'mapbox://styles/mapbox/streets-v11',
        center: Clayton, // starting position [Clayton]
        zoom: 14 // starting zoom
    });}

map.on('mousemove', function (e) {
document.getElementById('info').innerHTML =
// e.point is the x, y coordinates of the mousemove event relative
// to the top-left corner of the map
JSON.stringify(e.point) +
'<br />' +
// e.lngLat is the longitude, latitude geographical position of the event
JSON.stringify(e.lngLat.wrap());
});


