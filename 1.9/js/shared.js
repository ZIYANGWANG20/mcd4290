// Shared code needed by the code of all three pages.

// Prefix to use for Local Storage.  You may change this.
var APP_PREFIX = "monash.eng1003.fencingApp";

// Array of saved Region objects.
var savedRegions = [];
var DEFAULT_MAX_FENCE_POSTS_DISTANCE = 4;
//var GeographicLib = require("./geographiclib"),
class PolygonArea{
    constructor(lat,lon,num){
        this._lat = lat;
        this._lon = lon;
        this._num = num;
    }
    /*set AddEdge(azi,s){
        this._azi =newazi;
        this._s = news;
    }*/
    set lat(newlat){
        this._lat = newlat;
    }
    set lon(newlon){
        this._lon = newlon;
    }
    set num(newnum){
        this._num = newnum;
    }
    Clear(){
        
    }
    
    get lat(){
        return this._lat;
    }
    get lon(){
        return this._lon;
    }
    get num(){
        return this._num;
    }
    
    AddPoint(lat,lon){
        this._lat=newlat;
        this._log=newlon;
        return [this.lat,this.lon]
    }
    TestPoint(lat,lon,reverse,sign){
        
    }
    Compute(reverse,sign){
    return r.perimeter
    }
    //TestPoint(){}
}

//let polygonInstance = new PolygonArea();
//polygonInstance.Clear();
    /*PolygonArea.prototype.Compute = function(reverse, sign) {
    var vals = {number: this.num}, t, tempsum, crossings;
    if (this.num < 2) {
      vals.perimeter = 0;
      if (!this.polyline)
        vals.area = 0;
      return vals;
    }
    if (this.polyline) {
      vals.perimeter = this._perimetersum.Sum();
      return vals;
    }
    t = this._geod.Inverse(this.lat, this.lon, this._lat0, this._lon0,
                           this._mask);
    vals.perimeter = this._perimetersum.Sum(t.s12);
    tempsum = new a.Accumulator(this._areasum);
    tempsum.Add(t.S12);
    crossings = this._crossings + transit(this.lon, this._lon0);
    if (crossings & 1)
      tempsum.Add( (tempsum.Sum() < 0 ? 1 : -1) * this._area0/2 );
    // area is with the clockwise sense.  If !reverse convert to
    // counter-clockwise convention.
    if (!reverse)
      tempsum.Negate();
    // If sign put area in (-area0/2, area0/2], else put area in [0, area0)
    if (sign) {
      if (tempsum.Sum() > this._area0/2)
        tempsum.Add( -this._area0 );
      else if (tempsum.Sum() <= -this._area0/2)
        tempsum.Add( +this._area0 );
    } else {
      if (tempsum.Sum() >= this._area0)
        tempsum.Add( -this._area0 );
      else if (tempsum < 0)
        tempsum.Add( -this._area0 );
    }
    vals.area = tempsum.Sum();
    return vals;
  };*/
class Region {
    constructor(regionId){
        this._location = [];
        this._SaveDataTime = new Date();
        this._SaveRegionId = regionId;
        this._area = areaCal;
        this._perimeters = periCal;
    }
}

class RegionList {
    constructor(){
        this._Regions = [];
        this._NumberOfRegions = 0;
       }
    
    addRegion(region) {
        this._Regions.push(region);
    }
    
    GetRegion(id) {
        return this._Regions.find(r => r._SaveRegionId == id);
    }
    
    RemoveRegion(id) {
        this._Regions.slice(a => a._SaveRegionId == id, 1);
    }
    
    GetNumberOfRegions() {
        return this._Regions.length;
    }
}
let regionList = new RegionList();